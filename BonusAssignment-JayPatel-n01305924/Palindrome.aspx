﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="BonusAssignment_JayPatel_n01305924.Palindrome" %>
<asp:Content ID="Question" ContentPlaceHolderID="Content1" runat="server">
  
</asp:Content>
<asp:Content ID="userInputControls" ContentPlaceHolderID="Content2" runat="server">
    <div>
        <asp:Label runat="server">Enter string to be checked. </asp:Label>
        <asp:TextBox runat="server" ID="palindrome"></asp:TextBox>
        <asp:RequiredFieldValidator ID="palindromeFieldValidator" runat="server" ControlToValidate="palindrome" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="palindromeRegularExpression" runat="server" ControlToValidate="palindrome" ErrorMessage="Please enter a valid string to proceed." ValidationExpression="[a-zA-Z ]+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Button ID="submitString" Text="Check string for Palidrome?" runat="server" OnClick="Check_palindrome"/>
    </div>
</asp:Content>
<asp:Content ID="resultArea" ContentPlaceHolderID="Content3" runat="server">
    <div id="result" runat="server"></div>
</asp:Content>
