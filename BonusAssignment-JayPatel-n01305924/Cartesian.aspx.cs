﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_JayPatel_n01305924
{
    public partial class Cartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Check(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            double xaxisValue = Convert.ToDouble(XAxis.Text);
            double yaxisValue = Convert.ToDouble(YAxis.Text);

            if (xaxisValue > 0 && yaxisValue > 0)
            {
                result1.InnerHtml = "<strong>Result:</strong> (" + XAxis.Text + "," + YAxis.Text + ") lies in Quadrant 1";
            }
            else if (xaxisValue < 0 && yaxisValue > 0)
            {
                result1.InnerHtml = "<strong>Result:</strong> (" + XAxis.Text + "," + YAxis.Text + ") lies in Quadrant 2";
            }
            else if (xaxisValue < 0 && yaxisValue < 0)
            {
                result1.InnerHtml = "<strong>Result:</strong> (" + XAxis.Text + "," + YAxis.Text + ") lies in Quadrant 3";
            }
            else if (xaxisValue > 0 && yaxisValue < 0)
            {
                result1.InnerHtml = "<strong>Result:</strong> (" + XAxis.Text + "," + YAxis.Text + ") lies in Quadrant 4";
            }
        }

        protected void CartesianXAxisCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(XAxis.Text) == 0)
            {
                args.IsValid = false;
            }
        }
        protected void CartesianYAxisCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(YAxis.Text) == 0)
            {
                args.IsValid = false;
            }
        }

    }
}

    
