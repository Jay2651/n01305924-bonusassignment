﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_JayPatel_n01305924
{
    public partial class Primenumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void Check_PrimeNumber(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            int PInput = int.Parse(userInputNumber.Text);
        
            bool flag = false;

            for (int i = PInput - 1; i > 1; i--)
            {
                if (PInput % i == 0)
                {
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
                result.InnerHtml = "<strong> Result:</strong> \"" +PInput + "\" is a Prime Number";
            }
            else
            {
                result.InnerHtml = "<strong> Result:</strong> \"" + PInput + "\" is not a Prime Number";
            }
        }

    }
}
