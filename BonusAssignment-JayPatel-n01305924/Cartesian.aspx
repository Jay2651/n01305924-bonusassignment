﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cartesian.aspx.cs" Inherits="BonusAssignment_JayPatel_n01305924.Cartesian" %>
<asp:Content ID="Question" ContentPlaceHolderID="Content1" runat="server">
    <div>
        <br />
        <br />
        Given input of two integers, create a server click function which prints a message to a blank div
        element of which quadrant the co-ordinate would fall. Validate your inputs to assume that x and
        y are non-zero (for the sake of simplicity).
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="userInputControls" ContentPlaceHolderID="Content2" runat="server">
    <div>
        <asp:Label runat="server">Enter x-axis value: </asp:Label>
        <asp:TextBox runat="server" ID="XAxis"></asp:TextBox>
        <asp:RequiredFieldValidator ID="XAxisField" runat="server" ControlToValidate="XAxis" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="XAxisFieldValidator" runat="server" ControlToValidate="XAxis" ErrorMessage="Try Again Not A Valid Number." ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="Blue" Display="Dynamic"></asp:RegularExpressionValidator>
       <asp:CustomValidator ID="XAxisCustomValidator" runat="server" ControlToValidate="XAxis" OnServerValidate="CartesianXAxisCustomValidator_ServerValidate" ErrorMessage="X-Axis shouldnot be zero." ForeColor="Blue" Display="Dynamic"></asp:CustomValidator>
        <br />
        <asp:Label runat="server">Enter y-axis value: </asp:Label>
        <asp:TextBox runat="server" ID="YAxis"></asp:TextBox>
        <asp:RequiredFieldValidator ID="YAxisRequired" runat="server" ControlToValidate="YAxis" ErrorMessage="Value required."></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="YAxisRegularExpression" runat="server" ControlToValidate="YAxis" ErrorMessage="Please enter a valid number to proceed." ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <asp:CustomValidator ID="YAxisCustomValidator" runat="server" ControlToValidate="YAxis" OnServerValidate="CartesianYAxisCustomValidator_ServerValidate" ErrorMessage="Y-Axis value must be a non-zero number." ForeColor="Red" Display="Dynamic"></asp:CustomValidator>
        <br />
        <br />
        <asp:Button ID="UserInput" Text="Check" runat="server" OnClick="Check"/>
    </div>
</asp:Content>
<asp:Content ID="result" ContentPlaceHolderID="Content3" runat="server">
    <div id="result1" runat="server"></div>
</asp:Content>
