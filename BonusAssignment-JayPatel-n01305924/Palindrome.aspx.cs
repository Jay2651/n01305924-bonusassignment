﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_JayPatel_n01305924
{
    public partial class Palindrome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            result.InnerHtml = "";
        }

        protected void Check_palindrome(object sender, EventArgs e)
        {

            //first check to make sure everything is valid
            if (!Page.IsValid)
            {
                return;
            }
            /* Reffered
           Tihanyi, B. (2012, March 20). Check if a string is a palindrome. Retrieved October 23, 2018, 
           from https://stackoverflow.com/questions/9790749/check-if-a-string-is-a-palindrome  */
            string Input = palindrome.Text;

            string LCase = Input.ToLower();
            string  NoSpaces = LCase.Replace(" ", string.Empty);
            string Reverse = string.Empty;
            int length = NoSpaces.Length;
            bool flag = false; 
            for (int i = 0; i < length / 2; i++)
            {
                if (NoSpaces[i] != NoSpaces[length - i - 1])
                {
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
                result.InnerHtml = "Output: \"" + Input + "\" is a Palindrome String";
            }
            else
            {
                result.InnerHtml = "Output: \"" + Input + "\" is not a Palindrome String";
            }
        }
    }
}
